import json
import pickle
import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression

vectorizer_path = "data/vectorizer.pickle"
logistic_regression_path = "data/logistic_regression.pickle"
json_path = "files/train-data.json"
responses_path = "files/responses.json"
stop_words_path = "files/stopWords"


def generate_reply(message, private):
    try:
        f = open(vectorizer_path, "rb")
        vectorizer = pickle.load(f)
        f.close()

        f = open(logistic_regression_path, "rb")
        logistic_regression = pickle.load(f)
        f.close()

    except:
        vectorizer, logistic_regression = train()

    response_array = vectorizer.transform([message])

    prediction = logistic_regression.predict_proba(response_array)

    max_value = np.max(prediction)

    response_class = np.where(prediction == max_value)[1][0]

    if max_value < 0.5:
        if private:
            response_class = -1
        else:
            response_class = -2

    responsesFile = open(responses_path, "r", encoding='utf-8')
    reader = json.load(responsesFile)
    for row in reader:
        if row["class"] == response_class:
            return row["response"]


def train():
    sentences = []
    classes = []

    with open(json_path, "r", encoding="utf-8") as sentecesFile:
        reader = json.load(sentecesFile)
        for row in reader:
            sentences.append(row["sentence"])
            classes.append(row["class"])

    fit_sentences = []
    last_class = -1
    i = 0
    for response_class in classes:
        if response_class > last_class:
            last_class = response_class
            fit_sentences.append(sentences[i])
        else:
            fit_sentences[response_class] += str(" " + sentences[i])
        i += 1

    vectorizer = TfidfVectorizer(stop_words=stopWords(stop_words_path), strip_accents='ascii')
    vectorizer.fit(fit_sentences)
    vector_matrix = vectorizer.transform(sentences)

    logistic_regression = LogisticRegression(random_state=1)
    logistic_regression.fit(vector_matrix.toarray(), classes)

    f = open(vectorizer_path, "wb")
    pickle.dump(vectorizer, f)
    f.close()

    f = open(logistic_regression_path, "wb")
    pickle.dump(logistic_regression, f)
    f.close()

    return vectorizer, logistic_regression


def stopWords(file_name):
    vocabulary = open(file_name, "r", encoding="utf-8")
    spanishStopWords = vocabulary.read().splitlines()
    vocabulary.close()

    return spanishStopWords
