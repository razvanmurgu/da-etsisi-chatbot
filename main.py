import constants as keys
from telegram.ext import *
import vocabulary_processor as vp
import json

print("Bot started...")

vectorizer_path = vp.vectorizer_path
logistic_regression_path = vp.logistic_regression_path
json_path = vp.json_path
responses_path = vp.responses_path
stop_words_path = vp.stop_words_path


def start_command(update, context):
    update.message.reply_text('Hola! Me puedes preguntar sobre cualquier tema de la ETSISI e intentaré ayudarte.\n\nTambién puedes usar el comando /categorias para saber todo sobre lo que sé responder en este momento.')


def help_command(update, context):
    update.message.reply_text('Pregúntame sobre cualquier tema relacionado con la ETSISI.\n\nSé contestar a los temas más genéricos (puedes ver cuáles con /categorias), por lo que si tu pregunta es sobre un caso muy específico, lo mejor es que te pongas en contacto con mis compañeros de delegación en el @forodaetsisi')


def list_categories_command(update, context):
    categories = u"Las categorías que sé contestar actualmente son:\n"

    with open(responses_path, "r", encoding="utf-8") as sentecesFile:
        reader = json.load(sentecesFile)
        for row in reader:
            if row["class"] >= 0:
                categories = categories + "\n" + row["name"]

    update.message.reply_text(categories)


def info_command(update, context):
    update.message.reply_document(document='BQACAgQAAxkBAAIFNGEzsyRrcMzm39JPYKysBbq9Doa6AAKNCgAC4IugUZcUEYY3YVvWIAQ', caption='Este Bot fue creado como Proyecto de Fin de Grado por @razvanvm en 2021.\n\nEl código fuente está disponible en https://gitlab.com/razvanmurgu/da-etsisi-chatbot y la memoria del proyecto se adjunta en el mensaje.')


def handle_message(update, context):

    if update.message.chat.type == "private":
        message = str(update.message.text)

        response = vp.generate_reply(message, True)

        update.message.reply_text(response)

    else:
        message = str(update.message.text).lower()

        if message == "@" + context.bot.username and update.message.reply_to_message is not None and update.message.reply_to_message.from_user.username != context.bot.username:
            response = vp.generate_reply(str(update.message.reply_to_message.text), False)

            update.message.reply_text(response, reply_to_message_id=update.message.reply_to_message.message_id)

        elif "@" + context.bot.username in message:
            message = str(update.message.text).replace("@" + context.bot.username, '').strip()
            if message != '':
                response = vp.generate_reply(message, False)

                update.message.reply_text(response)

        elif update.message.reply_to_message is not None and update.message.reply_to_message.from_user.username == context.bot.username:
            if "@" not in update.message.text or ("@" in update.message.text and "@" + context.bot.username in update.message.text):
                response = vp.generate_reply(str(update.message.text), False)

            update.message.reply_text(response)


def error(update, context):
    print(f"Update {update} caused errror {context.error}")


def main():
    vp.train()

    updater = Updater(keys.API_KEY)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start_command))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(CommandHandler("categorias", list_categories_command))
    dp.add_handler(CommandHandler("info", info_command))

    dp.add_handler(MessageHandler(Filters.text, handle_message))

    dp.add_error_handler(error)

    updater.start_polling()

    updater.idle()


main()
