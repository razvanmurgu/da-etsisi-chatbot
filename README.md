﻿# Welcome to DA-ETSISI Chatbot!

Hi! I'm Razvan Murgu and this is my final degree project. It is a Telegram chatbot that helps ETSISI's students to get the information they need. The chatbot only works in Spanish.


# Instructions for testing the bot

You can test the bot now on https://t.me/razvanvmbot, but if you want to make changes on it, you can fork the entire repository. In order to make the bot work, you have to create a file called "constants.py" int the root folder with a variable in it called "API_KEY", just like this:

    API_KEY = 'your_bot_key'

If you don't know how to obtain that key, check the Telegram botfather in https://t.me/botfather.


In addition, you have to add your training data and the Bot responses in the directory /files, with JSON format. There should be two files, train-data.json and responses.json.

train-data.json should have the following format:

    [
      {
        "sentence": "train sentence 1",
        "class": 0
      },
      {
        "sentence": "train sentence 2",
        "class": 1
      },
      {
        "sentence": "train sentence 3",
        "class": 2
      }
    ]

responses.json should have the following format:

    [
      {
        "name": "Default reply for groups",
        "class": -2,
        "response": "Some default reply"
      },
      {
        "name": "Default reply for private chats",
        "class": -1,
        "response": "Some default reply"
      },
      {
        "name": "Class name 1",
        "class": 0,
        "response": "Response 1"
      },
      {
        "name": "Class name 2",
        "class": 1,
        "response": "Response 2"
      }
    ]